# Boussole numérique 3-Axes IC HMC5883L

[Specification du constructeur Honeywell](http://www51.honeywell.com/aero/common/documents/myaerospacecatalog-documents/Defense_Brochures-documents/HMC5883L_3-Axis_Digital_Compass_IC.pdf) 
![ 
](http://smart-prototyping.com/image/data/2_components/Arduino/100855%20GY-273%20HMC5883L%203%20Axis%20Compass%20Magnetometer%20Sensor%20Module/1.jpg)
Prix moyen constaté du module GY-273 : 3€ (1€ direct Chine)

## Utiliser le HMC5883L comme une boussole numérique

Le composant actif de motre module GY-273 l'IC HMC5883L, restitu sous la forme d'un vecteur de force magnétique les composantes orthogonales de l'intensité du champ (X, Y, et Z);
La connaissance du champ magnétique de la Terre (vecteur X et vecteur Y) permet de déterminer la direction du méridien magnétique, produisant ainsi une boussole numérique. 
Je vous propose un petit rappel sur la détermination d'un point géographique à l'aide d'une bousole magnetique.

### Éléments du champ magnétique

Le champ magnétique terrestre est une quantité vectorielle : il possède, à chaque point de l'espace, une intensité et une direction particulières. Trois valeurs sont nécessaires pour décrire précisement un point dans l'espace. Ces valeurs peuvent s'exprimer selon les formulations suivantes :  

* Les composantes orthogonales de l'intensité du champ (`X`, `Y`, et `Z`);
* L'intensité totale du champ et deux angles (`F`, `D`, `I`); ou
* Deux composantes de l'intensité du champ et un angle (`H`, `Z`, `D`)

Nous avons donc 7 composantes spécifiant le champ magnétique terrestre (`X`, `Y`, `Z`, `F`, `D`, et `H`)  
Définition et description de ces composantes :

| Composante  |  Description |
|:----------:|--------------|
|__X__   |**_C_**_omposante nord du vecteur champ magnétique; X est positif vers nord_|
|__Y__   |**_C_**_omposante est du vecteur champ magnétique; Y est positif vers l'est_|
|__Z__   |**_C_**_omposante verticale du vecteur champ magnétique; par convention, Z est positif vers le bas_|
|__D__   |**_D_**_éclinaison magnétique, définie comme étant l'angle entre le nord vrai (nord géographique) et le nord magnétique (la composante horizontale du champ magnétique), est positif vers le nord vrai_|
|__I__   |**_I_**_nclinaison magnétique, soit l'angle que fait le vecteur champ magnétique par rapport au plan horizontal et dont la valeur est positive vers le bas_|
|__H__   |**_I_**_ntensité horizontale du vecteur champ magnétique_|
|__F__   |**_I_**_ntensité totale du vecteur champ magnétique_| \**



En connaissant 3 de ces composantes on peut en déterminer les autres.    
La relation entre ces sept éléments est montrée dans le diagramme suivant.  

![diagramme](http://geomag.nrcan.gc.ca/images/field/fig04.gif)

Les sept éléments sont reliés entre eux par ces expressions simples :

|Composante| Expresion calculé|Remarque|
|----------|------------------|--------|
|Déclinaison (D)|D = atan(Y / X)| *atan : Arc tangente
|Inclinaison (I)|I = atan(Z / H)|*atan : Arc tangente
|Horizontale (H)|H = sqr(X² + Y²)| *sqr : racine carré
|Nord (X)|X = H x cos(D)|
|Est (Y)|Y = H x sin(D)|
|Intensité (F)|F = sqr(X² + Y² + Z²)|*sqr : racine carré

Voila pour cette petite intro théorique.



## Le capteur magnetique HC5883L

Le HMC5883L est un magnétomètre 3 axes numérique, il permet de mesurer une large gamme de champ magnétique de la Terre, avec champs de mesure de ± 8 gauss. Sa résolution de 12 bits permet de mesurer avec une précision de 2 miligauss, avec une consommation de courant de seulement 100μA. Le HMC5883L communique avec le microcontrôleur par l'intermédiaire de bus I2C avec une fréquence maximale de 75 Hz en mesures continu.

|Plage de mesure |Résolution |Gain
|----------------|-----------|-----
|± 0.88 Ga|   0.73 mG| 1370
|± 1.3 Ga |   0.92 mG| 1090
|± 1.9 Ga |   1.22 mG| 820
|± 2.5 Ga |   1.52 mG| 660
|± 4 Ga   |   2.27 mG| 440
|± 4.7 Ga |   2.56 mG| 390
|± 5.6 Ga |   3.03 mG| 330
|± 8.1 Ga |   4.35 mG| 230

## Branchement à l'arduino Mini Pro

![schema-branchement](https://s-media-cache-ak0.pinimg.com/originals/c5/6f/ec/c56fecb3b93a23aa0a4e837200ed7df6.png)

Le module GY-273 possede un régulateur de tension 5V->3,3V cela permet de brancher le module invariablement sur un arduino 5V ou 3,3V.
La broche marqué SLC (horloge) est à raccorder sur la broche A5 (I2C Clock) de l'arduinp,
La broche du GY-273 SDA (Data) est à raccorder sur la broche A4 (I2C Data) de l'arduino.
Les boches GND et VCC se raccorde respectivement sur la masse et la tension d'alimentation (+3,3V ou 5V)

## Installation de la bibliothèque HMC5884L pour Arduino

Sur internet on trouve de nombreuses bibliothèques programmés spécifiquement pour l'utilisation de ce capteur. Pour les exemples présentés ici, je l'ai choisi d'utiliser une bibliothèque téléchargé à partir du site Internet d'Adafruit.

Cliquez [ici](localisation du fichier à télécharger) pour télécharger la bibliothèque. Une fois téléchargé, il faut extraire et installé le répertoire 'HMC5883L' directement dans le dossier 'librairie' de l'IDE Ardbuino.


## Programmation

### Première version de notre boussole numérique :

L'objet boussole nous est a tous assez familier. C'est un petit boîtier cylindrique avec une aiguille aimanté monté sur un axe, affecté par le champ magnétique terrestre, l'aiguille aligne selon les lignes de champ qui traversent la surface de la terre, cela permet d'indiquer le nord (pas celui des [ch'its](https://www.youtube.com/watch?v=Ypl0qgdAt2c)).

Ce que nous voulons atteindre avec Arduino est la création d'une boussole qui va nous donner la valeur 0° lorsque le capteur est pointé vers le nord magnétique, et de 180 ° quand le capteur est orienté sud.

Le code que nous utilisons est très commun sur Internet et vous pouvez le trouver avec de petites variations infinies. Je propose, car il est un exemple très utile pour comprendre comment utiliser ce capteur, de se familiariser avec les commandes de la bibliothèque HMC5883L (au demeurant cela permet également de découvrir l'utilisation du bus de communication I2C).

Dans un second temps nous utiliseront directement les commandes du bus I2C sans passé par la bibliothèque HMC5883L qui masque totalement les spécificités des instructions I2C.

Sous l’éditeur de l'arduino (après avoir installé la bibliothèque HMC5883L), nous allons crée notre première version de notre programme 'boussole'.

Pour notre première instruction, il faut appeler (importer) la librairie **\<HMC5883L.h\>** dans notre code.

     #include <HMC5883L.h>

La communication avec le capteur se fait en mode série avec le protocole I2C (2 fils __SDA__ et __CLK__). Pour cela nous allons simplement (pour le moment) utiliser la bibliothèque de l'Arduino dédie à cette action 'Wire'.

     #include <wire.h>

Une fois que nous avons déclaré l'utilisation des bibliothèques, nous allons créer 'instancier' notre boussole. Cette déclaration se fait en début du code pour que notre boussole soit une variable globale pour tout notre programme.

    HMC5883L boussole;

Le programme actif peut débuter avec les 2 blocs de code : setup() 'pour la séquence d'initiation' et loop() 'l'algorithme de notre code', ces 2 méthodes omniprésents dans tout programme (esquisse) Arduino.

Nous commençons, c'est logique, par écrire la méthode setup(). D'abord, nous activons le protocole de communication I2C entre Arduino et le magnétomètre.

    void setup() {
        Wire.begin();

De plus comme il est souhaitable d'afficher sur notre PC les valeurs relevés par le capteur magnétomètre, nous avons besoin d'initier le lien de communication entre l'arduino et le PC.

        Serial.begin(9600);
        Serial.println("Ouverture de la communication avec l'HMC5883L");

Maintenant que toute les communication sont en place, nous allons initialiser notre boussole.

        Boussole = HMC5883L();

Cette commande appel la méthode d'initialisation de la bibliothèque HMC5883L.

La prochaine étape configure le capteur en fonction de nos besoins. Il faut déterminer notre plage de mesure, en précisant le facteur d'amplification à utiliser. Et de préciser également le mode de mesure.

        Serial.println("Etallonage pour une Plage de mesure de +/- 1.3Ga");
        int error = Boussole.SetScale(1.3);
        if(error != 0)
            Serial.println(Boussole.GetErrorText(error));
        Serial.println("Configuration du mode de lecture continu.");
        error = compas.SetMeasurementMode(Measurement_Continuous);
        if(error != 0)
           Serial.println(Boussole.GetErrorText(error));

Nous venons de spécifier au capteur que nous allons utiliser une plage de mesure comprise entre -1,3 et +1,3 Gauss (Ga).
Les autre plages disponibles sur le capteur HMC5883L sont : 0.88, 1.3, 1.9, 2.5, 4.0, 4.7, 5.6 et 8.1 gauss. Cela dépends de la localisation géographique. Plus l’échelle choisie sera fine, plus l'on gagne en définition et donc en précision. La définition du gain doit être en relation avec la force du signal magnétique terrestre, qui est très fluctuant en fonction de votre localisation. voir les explication suivante :[magnetic_field](http://en.wikipedia.org/wiki/Earth's_magnetic_field) 

Nous allons maintenant nous occuper du corps actif de notre programme, a savoir la lecture continue des mesure du capteur dans la boucle de traitement principale.

    void loop(){
        MagnetometerRaw Valeurs_Bruts = Boussole.ReadRawAxis();

La fonction ReadRawAxis() renvoie la valeur brute du magnétomètre, sans aucune mise à l’échelle. Dans la phase d'initialisation de notre code, nous avons spécifié la plage de mesure à considérer. La cohérence demanderait à utiliser la fonction ReadScaledAxis(), elle restitue une mesure exprimé en Gauss conforme à la plage de mesure configuré. 
Cependant, dans ce premier exemple nous souhaitons simplement simuler l'orientation de l'aiguille d'une boussole, une lecture de valeur brute est alors suffisante.

Notre magnétomètre est capable de détecter simultanément des mesures sur les trois axes cartésiens. 
L'appel à la fonction ReadRawAxis() renferme ses mesures dans les attribues de l'objet renvoyer par la fonction. Il suffit d’interroger ses attribues de la façons suivantes :

        int xAxis = Valeurs_Bruts.XAxis;
        int yAxis = Valeurs_Bruts.YAxis;
        int zAxis = Valeurs_Bruts.ZAxis;


Bien sur, le corollaire existe également pour la fonction de lecture sur la grandeur physique (exprimer en Gauss). 

        MagnetometerScaled Valeurs_Gauss = boussole.ReadScaledAxis();
        int xAxis = Valeurs_Gauss.XAxis;
        int yAxis = Valeurs_Gauss.YAxis;
        int zAxis = Valeurs_Gauss.ZAxis;

Ces éléments seront à reprendre dans un prochain code.

Attaquons maintenant l’écriture de la simulation de l'aiguille de la boussole.  
_Petit rappel de Trigo :_   
L'aiguille de notre boussole est l'expression de l'angle résultant entre les forces des signaux magnétiques sur les axes du plan horizontal, X et Y. Cette angle résultant, est en trigo l'expression de l'arc tangente sur ses deux vecteurs Y et X.  

__atang(Y/X)__    

Dans le code cela revient à écrire :  

    float Aiguille_Rad = atan2(Valeurs_Bruts.YAxis, Valeurs_Bruts.XAxis);
    if(aiguille < 0)
            aiguille_rad += 2*PI;

L'expression de l'angle est retourné en radiant, Pour transformer cela en degré il suffit d'inclure la petite conversion suivante:  

        float Aiguille_Deg = aiguille_rad * 180/M_PI;


Pour profiter du résultat, il reste encore à le renvoyer sur la console série, le PC connecté à l'arduino.  

        Serial.println(Aiguille_Deg);
        delay(1000);


Un petit délais pour avoir le temps d’apprécier le résultat de chaque mesure.

__La compil du code que nous venons d’écrire ensemble.__

    #include <Wire.h>
    #include <HMC5883L.h>
     
    HMC5883L Boussole;
     
    void setup()
    {
        Wire.begin();
        Serial.begin(9600);
        Serial.println("Ouverture de la communication avec l'HMC5883L");
        Boussole = HMC5883L();
     
        Serial.println("Etallonage pour une Plage de mesure de +/- 1.3Ga");
        int error = Boussole.SetScale(1.3);
        if(error != 0)
            Serial.println(Boussole.GetErrorText(error));
     
        Serial.println("Configuration du mode de lecture continu.");
        error = Boussole.SetMeasurementMode(Measurement_Continuous);
        if(error != 0)
            Serial.println(Boussole.GetErrorText(error));
    }
     
    void loop()
    {
        MagnetometerRaw Valeur_Brut = Boussole.ReadRawAxis();
        float Aiguille_Rad = atan2(Valeur_Brut.YAxis, Valeur_Brut.XAxis);
        if(Aiguille_Rad < 0)
            Aiguille_Rad += 2*PI;
        float Aiguille_Deg = Aiguille_Rad * 180/M_PI;
        Serial.println(Aiguille_Deg);
        delay(1000);
    }

Sur votre console de supervision connecté à votre arduino, vous pouvez lire les valeurs de notre simulation de boussole. En conservant le capteur HMC5883L bien à l’horizontal tout en effectuant un mouvement de rotation, on peut constater une valeur à 0,   __C'est le Nord magnétique !__  

![resultat_mesure](https://s-media-cache-ak0.pinimg.com/originals/63/35/01/633501782fadf3d6bfb85905b48a4ff0.png)



## Correction de la déclinaison magnétique

La force et l'orientation du champ magnétique terrestre est a la fois variable et non uniformément répartie sur la surface du globe. mais est soumis à des variations continues dans l'espace et le temps. De plus, mais vous savez sûrement qu’il existe deux nords : le nord magnétique et le nord géographique. ce décalage est appelé la __Déclinaison magnétique terrestre__.  
Cette __déclinaison magnétique__ est, en un point donné sur la surface de la terre, l'angle formé entre la direction du pôle Nord géographique et le Nord magnétique, il s'agit donc d'un angle sur le plan horizontal du point d'observation, généralement noté __'d'__ ou __'δ'__ .   

_Représentation de la Déclinaison magnétique terrestre._
![Decli_mag_ter](http://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/Magnetic_declination.svg/540px-Magnetic_declination.svg.png)

_Évolution de la déclinaison entre 1590 et 1990_
![Evo_decli](http://upload.wikimedia.org/wikipedia/commons/4/43/Earth_Magnetic_Field_Declination_from_1590_to_1990.gif)

Il est donc primordiale, pour avoir un bon cap, de tenir compte de cette déviation magnétique. Il faut connaître cette déclinaison pour votre point de localisation géographique, rendez-vous pour cela sur le lien suivant :  

[Votre déclinaison magnétique](http://www.magnetic-declination.com/)

Pour Colmar cela donne : ![DecMag_Colmar](https://s-media-cache-ak0.pinimg.com/originals/6a/3e/8a/6a3e8ab39db35db22b58763763afaa5e.png)

Reste à apporter cette petite correction à notre code précédent.  
Transformons cette mesure exprimé en degré - Minute en valeur décimale:  

1°43' correspond à : 1 + (43 / 60 x 100) = 1,72 degrés décimale  

Puis en valeur Radian  

1,72 degrés correspond à : 1,72 x 180 / pi = 0.03 rad  

Cela correspond toutefois à une déclinaison très faible.  

On ajoute cette correction au code de la façon suivante :  

    float Angle_Declinaison = 0.03;
    Aiguille_Rad += Angle_Declinaison;

Mais on aurait pu très bien faire cette correction sur les valeurs en dégrées.  

    float Angle_Declinaison_Deg = 1.72;
    Aiguille_Deg += Angle_Declinaison_Deg;

Reste encore de faire une petite adaptation finale :  

    if (Aiguille_Deg < 0)
        Aiguille_Deg += 360
    if (Aiguille_Deg > 360)
        Aiguille_Deg -= 360
     
Voila ce que nous donne le code de la boucle de traitement principale : 

    void loop()
    {
        MagnetometerRaw Valeur_Brut = Boussole.ReadRawAxis();
        float Aiguille_Rad = atan2(Valeur_Brut.YAxis, Valeur_Brut.XAxis);
        if(Aiguille_Rad < 0)
            Aiguille_Rad += 2*PI;
        float Aiguille_Deg = Aiguille_Rad * 180/M_PI;

        float Angle_Declinaison_Deg = 1.72;
        Aiguille_Deg += Angle_Declinaison_Deg;   

        if (Aiguille_Deg < 0)
            Aiguille_Deg += 360
        if (Aiguille_Deg > 360)
            Aiguille_Deg -= 360

        Serial.println(Aiguille_Deg);
        delay(1000);
    }


