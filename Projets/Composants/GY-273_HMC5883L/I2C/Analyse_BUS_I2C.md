# Analyse du BUS I2C (Sur le projet Boussole)

## Principe du BUS I2C ![icone](./i2c_icone.png)

[Voir également sous Wikipedia](https://fr.wikipedia.org/wiki/I2C)  
   
Le bus I2C (Inter Integrated Circuit) a été développé au début des années 80 par Philips Semiconductors pour permettre de relier facilement à un microprocesseur les différents circuits d'un téléviseur moderne.  
![Schema_BUS_I2C](./i2c-diagram.png)  
  
### Caractéristiques  
Le bus I2C permet de faire communiquer entre eux des composants électroniques très divers grâce à seulement trois fils : un signal de données (SDA), un signal d'horloge (SCL) et un signal de référence électrique (masse).  
  
Ceci permet de réaliser des équipements ayant des fonctionnalités très puissantes (en apportant toute la puissance des systèmes micro-programmés) tout en conservant un circuit imprimé très simple par rapport un schéma classique (8 bits de données, 16 bits d'adresse + les bits de contrôle).  
  
Les données sont transmises en série à 100 Kbits/s en mode standard et jusqu'à 400 Kbits/s en mode rapide. Ce qui ouvre la porte de cette technologie à toutes les applications où la vitesse n'est pas primordiale.  
  
De nombreux fabricants ayant adopté le système, la variété des circuits disponibles disposant d'un port I2C est énorme : ports d'E/S bidirectionnels, convertisseurs A/N et N/A, mémoires (RAM, EPROM, EEPROM,...), circuits audio (égaliseur, contrôle de volume,...) et autre drivers (LED, LCD,...).  
  
Le nombre de composants qu'il est ainsi possible de relier est essentiellement limité par la charge capacitive des lignes SDA et SCL : 400 pF.  
  
 

## Principes
Afin de d'éviter les conflits électriques, les Entrées/Sorties SDA et SCL sont de type "collecteur ouvert".  
  
#### Voici un schéma de principe :  

![Schéma de principe](./schema1.png)  
 _**Structure d'E/S d'un module I2C**_

### D'autres bus trifilaires  
- Le CBus de Phillips est l’ancêtre du bus I2C.  
- Le Bus SPI de Motorola 
- Le Bus µWire de National SemiConductor.

 

## Détail du protocole I2C   

C'est un Bus Multi-Maîtres/Multi-Esclaves, avec gestion de conflit que nous aborderons plus loin. Plusieurs circuits pouvant être branchés en même temps sur le même bus. Pour que cela fonctionne une organisation d’échange est établi, cela pour ordonner la prise de parole simultanée des différents modules. C'est le protocole I2C. Celui-ci définit la succession des états logiques possibles sur SDA et SCL, et la façon dont doivent réagir les circuits en cas de conflits.  

### Prise de contrôle du bus  
Pour prendre le contrôle du bus, il faut que celui-ci soit au repos (SDA et SCL à '1'). Pour transmettre des données sur le bus, il faut donc surveiller deux conditions particulières : la condition de départ (SDA passe à '0' alors que SCL reste à '1') et la condition d'arrêt (SDA passe à '1' alors que SCL reste à '1').  
  
Lorsqu'un circuit prend le contrôle du bus après avoir vérifié qu'il était libre, il en devient le maître. C'est lui qui génère alors le signal d'horloge.  

![Prise du bus](./schema2.png)  
_**Exemple de condition de départ et d'arrêt**_  
   

### Transmission d'un octet  
Après avoir imposé la condition de départ, le maître applique sur SDA le bit de poids fort D7. Il valide ensuite la donnée en appliquant pendant un instant un niveau '1' sur la ligne SCL. Lorsque SCL revient à '0', il recommence l'opération jusqu'à ce que l'octet complet soit transmis. Il envoie alors un bit ACK à '1' tout en scrutant l'état réel de SDA. L'esclave doit alors imposer un niveau '0' pour signaler au maître que la transmission s'est effectuée correctement. Les sorties de chacun étant à "collecteur ouvert", le maître voit le '0' et peut alors passer à la suite.  

![transmission octet](./schema3.png)  
_**Exemple de transmission réussie**_  

Dans cet exemple : SCL (horloge imposée par le maître), SDAM (niveaux de SDA imposés par le maître), SDAE (niveaux de SDA imposés par l'esclave), SDAR (niveaux de SDA réels résultants).  

   

### Transmission d'une adresse  
Comme l'un peut interconnecter bien plus de 2 modules sur le bus, il est nécessaire de définir pour chacun une adresse unique. L'adresse d'un circuit, codée sur sept bits, définissant le type du module et un numéro d'ordre dans le type, ce dernier est souvent défini par l'état appliqué à un certain nombre de ses broches (Voir plus loin . Cette adresse est transmise sous la forme d'un octet, défini comme ainsi.   

![transmission adresse](./schema4.png)  
_**Exemple d'octet d'adresse**_  

Les bits D7 à D1 représentent les adresses A6 à A0, et que le bit D0 est remplacé par le bit de R/W qui permet au maître de signaler s'il veut lire ou écrire une donnée. Le bit d'acquittement ACK fonctionne comme pour une donnée, ceci permet au maître de vérifier si l'esclave est disponible.  
  
#### Cas particulier des mémoires  
  
L'espace adressable d'un circuit de mémoire étant sensiblement plus grand que la plupart des autres types de circuits, l'adresse d'une information y est codée sur deux octets ou plus. Le premier représente toujours l'adresse du circuit, et les suivants l'adresse interne de la mémoire.  
  
#### Les adresses réservées  
  
Les adresses 00000XXX et 111111XX sont réservés à des modes de fonctionnement particuliers.   
  
 

### Écriture d'une donnée  
L'écriture d'une donnée par le maître ne pose pas de problème particulier :   

![écriture donnée](./schema5.png)   
_**Exemple d'écriture d'une donnée**_  
 

#### Cas particulier d'utilisation d'ACK  
  
L'écriture d'un octet dans certains composants (mémoires, micro-contrôleur,...) peut prendre un certain temps. Il est donc possible que le maître soit obligé d'attendre l'acquittement ACK avant de passer à la suite.  

   

### Lecture d'une donnée  
La lecture d'une donnée par le maître se caractérise par l'utilisation spéciale qui est faite du bit ACK. Après la lecture d'un octet, le maître positionne ACK à '0' s'il veut lire la donnée suivante ( cas d'une mémoire par exemple ) ou à '1' la cas échéant. Il envoie alors la condition d'arrêt.  

![lecture donnée](./schema6.png)  
_**Exemple de lecture d'une donnée**_  

   

## Gestion des conflits   

 

### Mise en situation  
La structure même du bus I2C a été conçue pour pouvoir y accueillir plusieurs maîtres. Se pose alors le problème commun à tous les réseaux utilisant un canal de communication unique : la prise de parole. En effet, chaque maître pouvant prendre possession du bus dès que celui-ci est libre, il existe la possibilité que deux maîtres prennent la parole en même temps. Si cela ne pose pas de problème sur le plan électrique grâce à l'utilisation de collecteurs ouverts, il faut pouvoir détecter cet état de fait pour éviter la corruption des données transmises.  
  
 

### Principe  
Comme nous l'avons vu précédemment, pour prendre le contrôle du bus, un maître potentiel doit d'abord vérifier que celui-ci soit libre, et qu'une condition d'arrêt ait bien été envoyée depuis au moins 4,7µs. Mais il reste la possibilité que plusieurs maîtres prennent le contrôle du bus simultanément.  
  
Chaque circuit vérifie en permanence l'état des lignes SDA et SCL, y compris lorsqu'ils sont eux même en train d'envoyer des données. On distingue alors plusieurs cas :  

- Les différents maîtres envoient les mêmes données au même moment ; les données ne sont pas corrompues, la transmission s'effectue normalement, comme si un seul maître avait parlé ; ce cas est rare.
- Un maître impose un '0' sur le bus ; il relira forcément '0' et continuera à transmettre. Il ne peut pas alors détecter un éventuel conflit.
- Un maître cherche à appliquer un '1' sur le bus ; s'il ne relit pas un niveau '1', c'est qu'un autre maître a pris la parole en même temps ; le premier perd alors immédiatement le contrôle du bus, pour ne pas perturber la transmission du second ; il continue néanmoins à lire les données au cas où celles-ci lui auraient été destinées.
   

### Exemple  
 Soit le chronogramme suivant :  

![exemple](./schema7.png)  

#### Dans cet exemple :  

- __SCLR__ : Horloge résultante.
- __SDA1__ : Niveaux de SDA imposés par le maître n°1.
- __SDA2__ : Niveaux de SDA imposés par le maître n°2.
- __SDAR__ : Niveaux de SDA réels résultants lus par les deux maîtres.
  
#### Analyse  
  
Lors du deuxième octet, le maître n°2 cherche à imposer un '1' (SDA2) , mais relit un '0' (SDAR), il perd alors le contrôle du bus et devient esclave (Cas n°3) . Il reprendra le contrôle du bus, lorsque celui-ci sera de nouveau libre. Le maître n°1 ne voit pas le conflit et continue à transmettre normalement (Cas n°2). Au total, l'esclave a reçu les données du maître n°1 sans erreurs et le conflit est passé inaperçu.  
  
 

## Évolutions   
  
 

Afin de compenser quelques lacunes des premières spécifications du bus I2C (qui datent de 1982), quelques nouvelles améliorations ont été apportées à partir de 1993 :   

- Le mode rapide : le bus a désormais la capacité de transmettre des données jusqu'à une vitesse de 400 Kbit/s.  
- Des entrées à triggers de Schmitt afin de limiter la sensibilité au bruit.  
- La mise en haute impédance d'un circuit non alimenté : ceci évite de bloquer le bus si un périphérique n'est pas alimenté.  
- Extension à 10 bits de l'adressage des circuits : l'adressage d'un circuit se fait maintenant sur 10 bits répartis dans deux octets d'adresse de la façon ci-dessous.
  
     ![Nouvelles caractéristiques](./schema8.png)  
  
  
 

## Adresses réservées    

   

Les adresses 0000 0XXX ne sont pas utilisées pour l'adressage de composants. Elles ont été réservées par Phillips pour effectuer certaines fonctions spéciales.  

- __0000 0000 : adresse d'appel général__  
Après l'émission d'un appel général, les circuits ayant la capacité de traiter ce genre d'appel émettent un acquittement.  
Le deuxième octet permet définir le contenu de l'appel :  

- __0000 0110 : RESET__  
Remet tous les registres de circuits connectés dans leur état initial __*(Mise sous tension)*__. Les circuits qui le permettent rechargent leur adresse d'esclave.  
  
- __0000 0010__  
Les circuits qui le permettent rechargent leur adresse d'esclave.  
  
- __0000 0100__  
Les circuits définissant leur adresse de façon matériel réinitialisent leur adresse d'esclave.  
  
- __XXXX XXX1__  
Cette commande joue le rôle d'interruption. XXXX XXX1 peut être l'adresse du circuit qui a généré l'interruption.  
  
- __0000 0001: octet de start__  
Cet octet est utilisé pour synchroniser les périphériques lents avec les périphériques rapides.  
  
- __0000 001X : début d'adressage CBus__  
L'émission de cet octet permet de rendre sourd tous les circuits I2C présents sur le bus. A partir de ce moment, on peut transmettre ce que l'on désire sur le bus, en utilisant par exemple un autre protocole. Le bus repasse en mode normal lors de la réception d'une condition d'arrêt.  
  
- __0000 0110 à 0000 1111__  
Ces octets ne sont pas définis et sont ignorés par les circuits I2C. Ils peuvent être utilisés pour débugger un réseau multi-maître.  

## Conclusion

Après ce très rapide focus sur le protocole I2C que pouvons nous conclure :  
Venant un monde plus informatique qu'électronique, quand on aborde une notion de protocole, je m'attend à avoir une notion de règle et de vocabulaire de communication.   
Hors il n'en est rien, I2C est un protocole électronique, c'est à dire qu'il ne traitant que la couche des signaux électronique et non un vocabulaire de communication.  
I2C définie à quel niveau de signal raccorder les differents équipement, mais ne déternine absolument pas les expressions, les ordres, les verbes, à utiliser pour communiquer avec un équipement ou un autre.  
Si l'on souhaite communiquer efficacement 'directement' avec un module il est impératif de se référer à la documentation technique du constructeur de ce module (la datasheet de la puce I2C).  
Soit on utilise une librairie de dialogue qui établie la couche d'abstraction 'de dialogue' entre environnement que l'on utilise (Raspberry-Pi, Arduino, où autres) et le module que l'on veux piloter. En l’occurrence pour notre exemple une librairie Arduino pour le module HMC5883L.   

Comme pour la suite de cette étude je souhaite 'pour la beauté de l’apprentissage' communiquer en mode direct avec le module , la lecture préalable de la [datasheet suivante](http://www.soc-robotics.com/pdfs/HMC5883L.pdf) s’avère nécessaire.   
La partie la plus pertinente débute à la page 10 concernant l'adresse des registres internes du module, ainsi que le paragraphe suivant 'I2C COMMUNICATION PROTOCOL'.  

Je vous propose maintenant de passer sur notre étude de cas avec le module boussole HMC5883L.  

_Pour approfondir vos connaissance sur le Bus I2C, je ne peut que vous conseiller le site qui ma servi de référence pour cette premiere partie : [site I2C fr](http://www.aurel32.net/elec/i2c.php)_  
_Et pour une doc récente (04/2014), complète et officielle : [Doc_NXP_I2C](http://www.nxp.com/documents/user_manual/UM10204.pdf)_  


# Étude sur un cas pratique : Le module boussole HMC5883 via l'analyseur BUS PIRATE  

rem : Je ne fais pas ici la description d'utilisation de la carte d'analyse "BUS PIRATE"  
  
## Configuration de la carte en mode I2C :  
Dans un terminal sous Linux je préconise l'utilisation de minicom  
(préciser l'utilisation de minicom dans ce cas)

On opéré de la façon suivante pour configure la carte BP en mode I2C 100KHz

- m <- pour accéder au menu  
- 4 <- pour sélectionner I2C  
- 3 <- pour sélectionner la vitesse de communication 100KHz  

Le terminal affiche alors le prompt I2C :

    I2C>

A ce stade le module HMC5883 doit être connecté à la carte 'Bus Pirate'  

Rem : Pour chaque module 

1) Première étape : On scanne le bus pour remonter les adresse des modules ainsi découverts 

La commande '(1)' lance une recherche de la présence de module I2C sur le bus, elle renvoi la liste des adresses I2C ainsi découverte sur le Bus.  

    I2C> (1)    
    Searching I2C address space. Found devices at:  
    0x3C(0x1E W) 0x3D(0x1E R)  

Cela nous indique que notre module se trouve à l'adresse hexa '3C' sur le bus I2C

2) Je vais maintenant procéder au paramétrage du module HMC5883L.
Commençons par fixer le taux échantillonnage à 3Hz.
Pour cela me me référer au manuel du module (datasheet). Page 11 'Configuration du registre A' on trouve le tableau suivant :

DO2|DO1|DO0|Typical Data Output Rate (Hz)
|---|---|---|---|
|0|0|0|0.75|
|0|0|1|1.5|
|0|1|0|3|
|0|1|1|7.5|
|1|0|0|15 (Default)|
|1|0|1|30|
|1|1|0|75|
|1|1|1|Not used|

L'adresse du registre A se trouve lui à l'adresse hexa '00' (voir 'Table2: Redister List' de la Datasheet)  

Nous disposons maintenant de la séquence qui va nous permettre de fixer le taux d’échantillonnage du module HMC5883L à 3Hz :  
Pour cela il suffit d’écrire sur notre module I2C à l'adresse '0x3C' sur le registre 'A' à l'adresse '0x00' la valeur '0x20'.  

    I2C> [0x3c 0x00 0x20]
    I2C START BIT
    WRITE: 0x3C ACK
    WRITE: 0x00 ACK
    WRITE: 0x70 ACK
    I2C STOP BIT

3) En suivant la même logique, je fixe le gain à 5 (je fixe à 5 soit le registre B - Voir le datasheet)

    I2C>[0x3c 0x01 0xa0] 
    I2C START BIT
    WRITE: 0x3C ACK
    WRITE: 0x01 ACK
    WRITE: 0xA0 ACK
    I2C STOP BIT

4) Je passe le module en mode mesure simple

    I2C>[0x3c 0x02 0x01]
    I2C START BIT
    WRITE: 0x3C ACK
    WRITE: 0x02 ACK
    WRITE: 0x01 ACK
    I2C STOP BIT

5) Et enfin je lis une séquence de 6 octets

    I2C>[0x3d r:6]
    I2C START BIT
    WRITE: 0x3D ACK
    READ: 0xFF ACK 0xFD ACK 0xFF ACK 0x98 ACK 0x00 ACK 0x5F NACK
    I2C STOP BIT

Whaou ; faut encore décoder les valeurs retournées (sans évoquer les ACK NACK)  
1er Mot de 2 Octect : 0xFF 0xFD ; Force du signal magnétique sur l'axe X  
2ém Mot de 2 Octect : 0xFF 0x98 ; Force du signal magnétique sur l'axe Y  
3ém Mot de 2 Octect : 0x00 0x5F ; Force du signal magnétique sur l'axe Z  

Attention comme il sagit de nombre entier signé, il sont donc représente sous la forme 'Complement à 2'.  
Voir sur le net pour plus de détail ex:  [Wikipedia C2](https://fr.wikipedia.org/wiki/Compl%C3%A9ment_%C3%A0_deux)

L'exercice avait l'objectif de se confronté un peut et en direct avec le bus I2C, Vous conviendrez qu'il est bien plus élégant d'utiliser une librairie toute faite qui va nous masquer toute cette tuyauterie technique d'accès au registre et comportement du module.  
C'est en effet tout le sens de l'utilisation des librairies, que de proposer une abstraction fonctionnelle à l'utilisation des modules techniques. 

Pour la suite on peut voir comment est implémenter l'une des nombreuses librairies dédier au module Magnétoélectrique  HMC5883L.

 [__HMC5883L 3-axis magnetometer__](http://www.i2cdevlib.com/devices/hmc5883l#source)

 Se sera pour une suite .....